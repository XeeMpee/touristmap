<?php

require_once 'AppController.php';
require_once dirname(__DIR__).'/Database.php';
require_once dirname(__DIR__).'/models/UserMapper.php';

class DefaultController extends AppController 
{
    public function __construct() {
        parent::__construct();
    }

    public function index()
    {   
        $this->render('index');
    }

    public function index2()
    {
        $this->render('index2');
    }

    public function login(Type $var = null)
    {

        $userMapper = new UserMapper();
        $user = null;
        
        
        if(isset($_POST['username']) && isset($_POST['password']))
        {
            $user = $userMapper->getUser($_POST['username']);
        }

        // var_dump(password_verify($_POST['password'], $user->getPassword()));
        // var_dump($user);
        // var_dump($user->getPassword());
        // var_dump($_POST['password']);
        // die();

        if($user->getId()) {
            if(password_verify($_POST['password'], $user->getPassword())){
                session_start();
                $_SESSION['user'] = $user;
                $this->render('index');
            } else {
                $var = ['message' => 'Wrong password...'];
                $this->render('index', $var);
            }
        }else {
            $var = ['message' => 'Wrong user name...'];
            $this->render('index', $var);
        }
    }

    public function signup()
    {
       
        if(isset($_POST['username']) 
        && isset($_POST['email'])
        && isset($_POST['password'])
        && isset($_POST['confirm_password']))
        {   
            $username = $_POST['username'];
            $email= $_POST['email'];
            $password= $_POST['password'];
            $confirm_password= $_POST['confirm_password'];

            $conditions = [
                'filled' => true,
                'password_confirmed' => true,
                'single' => true,
                'password_requirements' => true
            ];

            $render_variable = [];

            // Checking if not empty forms:
            foreach ($_POST as $key => $value) {
                if($value == '') {
                    $conditions['filled'] = false;
                    
                    if(!$render_variable['message']){
                        $render_variable['message'] = 'Please fill all fields in form...';
                    }
                    break;
                }
            }

            // Checking if user already exist:
            $database = new Database();
            $pdo = $database->connect();
            $query = "SELECT id FROM Users WHERE email = :email OR username = :username";
            $data = [
                'email' => $email,
                'username' => $username
            ];
            $results = null;

            try {
                $stmt = $pdo->prepare($query);
                $stmt->execute($data);                
                $results=$stmt->fetch(PDO::FETCH_ASSOC);
            } catch (PDOException $e) {
                echo ("Database getting data problem...");
            }

            if($results) {
                $conditions['single'] = false;
                if(!$render_variable['message']){
                    $render_variable['message'] = 'User with the same name or email already exists...';
                }
            }

            // Checking if passwords are the same:
            if($password !== $confirm_password) {
                $conditions['password_confirmed'] = false; 
                
                if(!$render_variable['message']){
                    $render_variable['message'] = 'Passwords are not the same...';
                }
            }

            // Checking password strenght:
            // TODO: password longer than 6signs, must have digits 

            // Checking if all conditions are met:
            $boolean = true;
            foreach ($conditions as $key => $value) {
                if($value == false){
                    $boolean = false;
                    break;
                }
            }
            
            // Adding new User:
            if($boolean == true)
            {
                $database = new Database();
                $pdo = $database->connect();

                $sql = 'INSERT INTO Users (username, email, password, role) VALUES (:username,:email,:password,:role)'; 
                $hashpsw = password_hash($password, PASSWORD_BCRYPT);
                // var_dump($hashpsw);
                // var_dump($password);
                // die();
                $data = [
                    'username' => $username,
                    'email' => $email,
                    'password' => $hashpsw,
                    'role' => 0
                ];

                try {
                    $stmt = $pdo->prepare($sql);
                    $stmt->execute($data);
                } catch (PDOException $e) {
                    echo("Error" . $e->message);
                }

            }
        }

        $this->render('registration', $render_variable);
    }

    public function user_page()
    {
        if(isset($_SESSION['user'])){
            $this->render('user_page');
        } else {
            $this->render('index');
        }
    }

    public function logout()
    {
        session_unset();
        session_destroy();
        $this->render('index');
    }
}

?>