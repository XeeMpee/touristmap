<?php


class AppController
{
    public function __construct() {
        session_start();
    }

    public function render(string $filename = null, $variables = null)
    {
        $view = $filename ? dirname('./AppController') . '/views/' . $filename . '.php' : null;
        
        $output = null;
        if(!file_exists($view)){
            $output = "No such page!";
            print($output);
        } else {
            ob_start();
            include($view);
            if($variables)
            {
                extract($variables);
            }
            
            $output = ob_get_clean();
            print($output);
        }
    }
}

?>