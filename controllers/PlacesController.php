<?php

require_once(dirname(__DIR__).'/models/PlaceMapper.php');
require_once(dirname(__DIR__).'/models/Place.php');

class PlacesController extends AppController {


    private $places = [];


    public function __construct() {
        parent::__construct();
    }

    public function webpage(Type $var = null)
    {
        $this->loadPlacesFromDataBase();
        $this->render('places', $this->places);
    }

    private function loadPlacesFromDataBase() {
        $mapper = new PlaceMapper();
        $this->places = $mapper->getAllPlaces(); 
    }

}

?>