<?php
require_once('AppController.php');

class PlaceController extends AppController 
{   
   
    public function __construct() {
        parent::__construct();
    }


    public function webpage()
    {


        if(isset($_GET['place'])) {
            
            $variables = null;
            $variables['place'] =$this->getPlace();
            $this->render('place', $variables);
        }
    }

    private function getPlace() : Place
    {
        $placeMapper = new PlaceMapper();
        $place = $placeMapper->getPlace($_GET['place']);        
        return $place;
    }
}

?>