<?php
require_once('Place.php');
require_once(dirname(__DIR__).'/Database.php');
class PlaceMapper
{
    private $database;
    public function __construct($database = null) {
        if($database == null) {
            $this->database = new Database();
        }
    }
    
    public function getPlace($id)
    {
    
        try {
            $pdo = $this->database->connect();
            $sql = 'SELECT * FROM Places WHERE id=:id';
            $stmt = $pdo->prepare($sql);
            $stmt->bindParam(":id",$id,PDO::PARAM_INT);
            $stmt->execute();
            $place = $stmt->fetch(PDO::FETCH_ASSOC);
            return new Place($place['id'],$place['name'],$place['longitude'], $place['latitude'],$place['description']);
        } catch (PDOException $e) {
            echo("Cant get place from database...");
            return null;
        }
    }

    public function getTopPlaces($n=3)
    {   
        try {
            $pdo = $this->database->connect();
            $sql = 'SELECT id, name, latitude, longitude, description,
            (SELECT avg(rate) FROM Rates WHERE Rates.place_id = Places.id) as average
            FROM Places ORDER BY AVERAGE DESC LIMIT ' . $n;
            $stmt = $pdo->prepare($sql);
            $stmt->execute();
            $place = $stmt->fetchAll(PDO::FETCH_ASSOC);
            
            $places = [];
            foreach ($place as $p) {
                $places[] = new Place($p['id'],$p['name'],$p['longitude'], $p['latitude'],$p['description']);
            }
            return $places;
        
        } catch (PDOException $e) {
            echo("Cant get top places from database...");
            return null;
        }
        
    }

    public function getAllPlaces() {
         try {
            $pdo = $this->database->connect();
            $sql = 'SELECT id, name, latitude, longitude, description,
            (SELECT avg(rate) FROM Rates WHERE Rates.place_id = Places.id) as average
            FROM Places ORDER BY AVERAGE DESC';
            $stmt = $pdo->prepare($sql);
            $stmt->execute();
            $place = $stmt->fetchAll(PDO::FETCH_ASSOC);
            
            $places = [];
            foreach ($place as $p) {
                $places[] = new Place($p['id'],$p['name'],$p['longitude'], $p['latitude'],$p['description']);
            }
            return $places;
        
        } catch (PDOException $e) {
            echo("Cant get places from database...");
            return null;
        }
        
    }
}


?>
