<?php

require_once dirname(__DIR__).'/Database.php';
require_once __DIR__.'/User.php';

class UserMapper 
{
   private $database;

    public function __construct(Database $database = null) {
        $this->database = $database==null ? new Database() : $database;
    }

   public function getUser(string $username):User
   {
       try {
            $pdo = $this->database->connect();
            $sql = 'SELECT * FROM Users WHERE username = :username';
            $stmt = $pdo->prepare($sql);
            $stmt->bindParam(':username', $username, PDO::PARAM_STR);
            $stmt->execute();
            $user = $stmt->fetch(PDO::FETCH_ASSOC);
            return new User($user['id'],$user['username'],$user['email'],$user['password'],$user['role']);

       } catch (PDOException $e) {
            echo("Database: get user problem....");
            return null;
       }
   }

   
   public function delete($id)
   {
        try {
            $pdo = $this->database->connect();
            $sql = 'DELETE FROM Users WHERE id=:id';
            $stmt = $pdo->prepare($sql);
            $stmt->bindParam(':id',$id,PDO::PARAM_STR);
            $stmt->execute();

        } catch (PDOException $e) {
            echo("Database: delete user problem");
        }
    }
}


?>