<?php


const DRIVER = 'mysql';
const HOST = 'localhost';
const DBNAME = 'tourist_map';
const USERNAME = 'root';
const PASSWORD = '';


class Database
{

    private $driver; 
    private $host; 
    private $dbname; 
    private $username; 
    private $password; 

    public function __construct(string $driver=DRIVER, 
    string $host=HOST, string $dbname=DBNAME, 
    string $username=USERNAME, string $password=PASSWORD)
    {
        $this->driver = $driver;
        $this->host = $host;
        $this->dbname= $dbname;
        $this->username= $username;
        $this->password = $password;

    }

    public function connect()
    {
        try {
            return new PDO($this->driver.":host=".$this->host.";dbname=".$this->dbname,$this->username,$this->password);
        } catch (PDOExceception $e) {
            echo ("Connection failed...");
        }
    }

}

?>