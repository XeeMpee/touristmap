<?php

require_once 'controllers/DefaultController.php';
require_once 'controllers/UserController.php';
require_once 'controllers/PlacesController.php';
require_once 'controllers/PlaceController.php';


class Routing
{

    private $routes = [];
    
    public function __construct() 
    {
        $this->routes = [
            'index' => [
                'controller' => 'DefaultController',
                'action' => 'index'
            ],

            'index2' => [
                'controller' => 'DefaultController',
                'action' => 'index2'
            ],

            'login' => [
                'controller' => 'DefaultController',
                'action' => 'login'
            ],

            'registration' => [
                'controller' => 'UserController',
                'action' => 'registration'
            ],

            'signup' => [
                'controller' => 'DefaultController',
                'action' => 'signup'
            ],

            'user_page' => [
                'controller' => 'DefaultController',
                'action' => 'user_page'
            ],

            'places' => [
                'controller' => 'PlacesController',
                'action' => 'webpage'
            ],

            'place' => [
                'controller' => 'PlaceController',
                'action' => 'webpage'
            ],

            'logout' => [
                'controller' => 'DefaultController',
                'action' => 'logout'
            ]
        ];
    }
    
    public function run()
    {
        $page = isset($_GET['page']) && isset($this->routes[$_GET['page']]) ? $_GET['page'] : 'index';

        if($this->routes[$page]) {
            $controler = $this->routes[$page]['controller'];
            $action = $this->routes[$page]['action'];

            $object = new $controler();
            $object->$action();
        }
    }
}

?>