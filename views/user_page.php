<!DOCTYPE html>
<?php
include "header.php";
?>
<head>
  <link rel="stylesheet" href="views/user_page.css" />
</head>
<body>

<?php include('navbar.php') ?>

<div class="container" id="user_info">
    <div class="container col-4 info-content" id="info-content-left">
        <h4><?=$_SESSION['user']->getUserName()?></h6>
    </div>

    <div class="container col-4 info-content" id="info-content-right">

    </div>
</div>

</body>

</html>