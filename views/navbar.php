<?php
require_once(dirname(__DIR__).'/models/User.php');
?>

<style>
#bar {
  position: sticky;
  top: 0px;
  z-index: 999;
  height:60px;
}

#bar > a {
  font-size: 25px;
  font-weight: bold;
}
</style>

<div id="bar">
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="bar">
  <a class="navbar-brand" href="#">NewTravel</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="?page=index">Home<span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="?page=places">Places</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Reportagues</a>
      </li>
    </ul>
  </div>

  <div id="user-navbar">
    <?php if(!isset($_SESSION['user'])) { ?>
      <a href="?page=index#user"><button class="btn btn-info">Login</button></a>
      <a href="?page=registration"><button class="btn btn-info">Sign up</button></a>
    <?php } else { ?>
      <a href="?page=user_page"><button class="btn btn-info"><?=$_SESSION['user']->getUserName();?></button></a>
      <a href="?page=logout"><button class="btn btn-info">Logout  </button></a>
    <?php } ?>
  </div>
</nav>
</div>
