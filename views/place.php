<!DOCTYPE html>
<html>
<?php include('header.php') ?>

<head>
    <link rel="stylesheet" href="views/place.css">
</head>

<body>
<?php include('navbar.php') ?> 
</body>

    <div class="container-fluid col-12" id="place-image">
        <img src="images/places/<?=$variables['place']->getId();?>_bg.png" alt="">
        <div class="row">
            <div id="place-miniature">
                <img src="images/places/<?=$variables['place']->getId();?>.png" alt="">
            </div>
            <div id="place-name">
                <h1>
                    <?=$variables['place']->getName(); ?>
                    <hr>    
                </h1>
            </div>
        </div>
    </div>

</html>
