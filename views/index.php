<!DOCTYPE html>

<html>
<?php
include "header.php";
require_once(dirname(__DIR__).'/Database.php');
require_once(dirname(__DIR__).'/models/Place.php');
require_once(dirname(__DIR__).'/models/PlaceMapper.php');
require_once('Database.php');
?>
<head>
  <link rel="stylesheet" href="views/index.css" />
</head>

<body>

<!-- Navbar -->
<?php include('navbar.php'); ?>


<!-- Carousel -->
<div id="myCarousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#myCarousel" data-slide-to="0" class=""></li>
          <li data-target="#myCarousel" data-slide-to="1" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="2" class=""></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active carousel-item-left">
            <img src="../images/slider/02_winter_forest.png" alt="image" class="carousel-image">
          </div>
          <div class="carousel-item carousel-item-next carousel-item-left">
            <img class="second-slide " src="../images/slider/01_sydney.png" alt="Second slide">
          </div>
          <div class="carousel-item">
            <img class="third-slide carousel-img" src="../images/slider/03_iceland.png" alt="Third slide">
          </div>
        </div>
        <a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>
  </div>
</div>

<!-- Map and user_panel -->

<div class="container row" id=map-user-panel>
  
    <div class="container col-8" id="map_container">
    
    </div>


    <div class="container col-4" id="user_panel">

    <?php if(!isset($_SESSION['user'])) { ?>
      <img src="../images/icons/nouser.png" class="user-image" alt="unregistered">
      <form class="from-group" action="?page=login" id="login-form" method="post">
      <br>Username:<br><input type="text" name="username">
      <br>Password:<br><input type="password" name="password"><br>
    
        <?php if($variables['message']){?>
                <div class="alert alert-danger alert-dismissible fade in col" id="my_alert">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Failture! </strong> <?=$variables['message'] ?>
                </div>
        <?php } ?>
        
        <button class="btn btn-info" type="submit">Login...</button>
        </form>
          
        <form action="?page=registration" method="post">
            <button class="btn btn-info" type="submit">Sign up</button>
        </form> 
        
      <?php } else { ?>
          <div id="user">
              <img src="../images/icons/user.png" class="user-image" alt="user_icon">
                  <a href='?page=user_page'>
                    <button class="btn btn-info user-buttons" type="submit"> 
                      <?= $_SESSION['user']->getUserName() ?> 
                    </button>
                  </a>
                  <a href="?page=logout"><button class="btn btn-info user-buttons" type="submit">Logout</button> </a>
            </div> 
      <?php } ?>

    </div>
</div>

<!-- Top places -->
<div class="jumbotron" id="top-week">
  <div class="container marketing">
        <div class="row">
          <?php 
          $mapper = new PlaceMapper();
          $places = $mapper->getTopPlaces(3);
          for ($i=0; $i < 3; $i++) {
          ?>
            <div class="col-lg-4">
                <img class="rounded-circle my-place-miniature" src="../images/places/<?php echo $places[$i]->getId();?>.png" alt="Generic placeholder image" width="140" height="140">
                <h2><?= $places[$i]->getName(); ?></h2>
                <p><?= substr($places[$i]->getDescription(),0,330) . '...';?></p>
                <p><a class="btn btn-secondary" href="#" role="button">Explore »</a></p>
            </div><!-- /.col-lg-4 -->
          <?php } ?>
      
        </div><!-- /.row -->
  </div>
</div>

<!-- Top reportagues -->
<div class="jumbotron" id="top_reportague">
  <div class="row">
    <div class="col-md-4">
      <h2>Heading</h2>
      <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
      <p><a class="btn btn-secondary" href="#" role="button">Read more... »</a></p>
    </div>
    <div class="col-md-4">
      <h2>Heading</h2>
      <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui. </p>
      <p><a class="btn btn-secondary" href="#" role="button">Read more... »</a></p>
    </div>
    <div class="col-md-4">
      <h2>Heading</h2>
      <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus.</p>
      <p><a class="btn btn-secondary" href="#" role="button">Read more... »</a></p>
    </div>
  </div>
</div>

<!-- Footer -->
<div class="row" id="footer">
  <p> FOOTER </p>
</div>


</body>
</html>