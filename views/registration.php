<!DOCTYPE html>

<?php
    include 'header.php';
?>

<header>
    <link rel="stylesheet" href="views/registration.css">
</header>

<body>
<?php include('navbar.php') ?>

    <div class="container" id="main">

        <?php
        if(isset($variables)){
             foreach ($variables as $key => $value) {
                echo'<div class="alert alert-danger alert-dismissible fade in col" id="my_alert">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Failture!   </strong>'. $variables['message']. '
                </div>';
            }

        }
        ?>

        <div class="container" id="registration">

            <p> <strong>REGISTRATION </strong></p>
            <hr>

            <form action="?page=signup" method="post">

                <div class="form-group mf">
                    <label for="exampleInputEmail1">Username:</label>
                    <input type="text" class="form-control" placeholder="Enter username" name="username">
                </div>    
            
               <div class="form-group mf">
                    <label for="exampleInputEmail1">Email address:</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" name="email">
                    <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                </div>
          
                <div class="form-group mf">
                    <label for="exampleInputPassword1">Password:</label>
                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password" title="Your password... I don`t care...">
                </div>
           
                <div class="form-group mf">
                    <label for="exampleInputPassword1">Confirm password:</label>
                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="confirm_password">
                </div>
          
         
                <button type="submit" class="btn btn-primary btn-block">Sign up</button>
          
            </form>

        </div>
    </div>

</body>