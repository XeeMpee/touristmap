<!DOCTYPE html>
<html>

<?php include('header.php')?>
<head>

<link rel="stylesheet" href="views/places.css" />
</head>
<body>
<?php include('navbar.php')?>

<div class="album py-5 bg-light">
    <div class="container">

      <div class="row">
        <?php for ($i=0; $i < count($variables); $i++) { ?>
        <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
            <img class="bd-placeholder-img card-img-top places_pictures" src="images/places/<?=$variables[$i]->getId();?>.png" alt="">
            <div class="card-body">
                <div class="card-text">
                <h5><?=$variables[$i]->getName();?></h5>
                <p><?=$variables[$i]->getDescription();?></p>
                </div>
              <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                  <a href="?page=place&place=<?=$variables[$i]->getId();?>"><button type="button" class="btn btn-sm btn-outline-secondary">View</button></a>
                  <button type="button" class="btn btn-sm btn-outline-secondary">Rate </button>
                  <button type="button" class="btn btn-sm btn-outline-secondary">Comment</button>
                </div>
                <!-- <small class="text-muted">9 mins</small> -->
                <?php if(isset($_SESSION['user']) && $_SESSION['user']->getRole()==1) { ?>
                <div class="btn-group" style="float:right;">
                  <button type="button" class="btn btn-sm btn-outline-secondary">Delete</button>
                  <button type="button" class="btn btn-sm btn-outline-secondary">Modify</button>
                </div>
                <?php } ?>
              </div>
            </div>
          </div>
        </div>
        <?php } ?>
        
        <?php if(isset($_SESSION['user']) && $_SESSION['user']->getRole()==1) { ?>
        <div id="plus">
          <a href="?page=add_pace"><div id="plus-icon" class="glyphicon glyphicon-plus"></div></a>   
        </div>
        <?php } ?>
  </div>

</body>
</html>